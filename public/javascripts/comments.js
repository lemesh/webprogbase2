const search = document.getElementById("searchInput");
const postList = document.getElementById("commentList");
const paginationPage = document.getElementById("pagination");

const headers = new Headers();
headers.append("Authorization", "Basic " + btoa("lemesh:Ol917364"));

let current_page = 1;
let max_page;

function paginationData(data) {
  if (data.pages !== 0) {
    document.getElementById("searchCount").innerHTML = data.total;
    const endpage =
      +data.page <= 3
        ? data.pages <= 5
          ? data.pages
          : 5
        : +data.page + 2 <= data.pages
        ? +data.page + 2
        : data.pages;
    const startpage =
      +data.page <= 3
        ? 1
        : +data.page + 2 >= data.pages
        ? data.pages - 4
        : +data.page - 2;
    const pages = [...Array(endpage - startpage + 1).keys()].map(x => {
      return {
        active: startpage + x === +data.page ? "btn-primary" : "btn-link",
        num: startpage + x
      };
    });
    return {
      pages,
      prev: +data.page === 1 ? "len" : "",
      next: +data.page === data.pages ? "len" : ""
    };
  }
}

function pag(page = 1, search = "") {
  current_page = page;
  Promise.all([
    fetch(`/api/v1/comments?page=${page}&search=${search}`, {
      method: "GET",
      headers
    }).then(res => res.json()),
    fetch("/templates/comments.mst").then(res => res.text()),
    fetch("/templates/pagination.mst").then(res => res.text())
  ])
    .then(([posts, templatePost, templatePag]) => {
      max_page = posts.pages;
      if (posts.pages !== 0) {
        return Promise.all([
          Mustache.render(templatePost, { comments: posts.docs }),
          Mustache.render(templatePag, paginationData(posts))
        ]);
      } else {
        return Promise.reject("No such comments");
      }
    })
    .then(([list, pagination]) => {
      postList.innerHTML = list;
      paginationPage.innerHTML = pagination;
      const pages = document.getElementsByClassName("btnPage");
      for (let i = 0; i < pages.length; i++) {
        pages[i].addEventListener(
          "click",
          () => {
            pag(+pages[i].innerHTML, search);
          },
          false
        );
      }
      const prevPage = document.getElementById("prev");
      const lastPage = document.getElementById("last");
      const nextPage = document.getElementById("next");
      const firstPage = document.getElementById("first");
      prevPage.addEventListener("click", () => {
        pag(current_page - 1);
      });
      lastPage.addEventListener("click", () => {
        pag(max_page);
      });
      nextPage.addEventListener("click", () => {
        pag(current_page + 1);
      });
      firstPage.addEventListener("click", () => {
        pag(1);
      });
    })
    .catch(err => console.error(err));
}

search.addEventListener("input", () => {
  document.getElementById("searchQuery").innerHTML = search.value;
  f;
  pag(1, search.value);
});

pag();
