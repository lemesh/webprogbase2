const router = require("express").Router();
const cloudinary = require("cloudinary");
const mongoose = require("mongoose");

const User = require("../models/User");

function checkAuth(req, res, next) {
  if (!req.user) return res.redirect("/auth/login");
  next();
}

router.get("/", checkAuth, (req, res) => {
  User.findOne({ username: req.user.username })
    .populate({
      path: "posts",
      populate: {
        path: "comments",
        populate: { path: "author" }
      }
    })
    .exec()
    .then(user => {
      const data = { userdata: user, user: req.user, title: req.user.fullname };
      if (req.user) data.title = req.user.fullname;
      res.render("profile", data);
    })
    .catch(err => res.status(400).send(err.toString()));
});

router.post("/", (req, res) => {
  if (req.files.avatarUrl) {
    const fileObject = req.files.avatarUrl;
    const fileBuffer = fileObject.data;
    cloudinary.v2.uploader
      .upload_stream({ resource_type: "raw" }, (err, result) => {
        if (err) return res.status(400).send(err.toString());
        User.findOneAndUpdate(
          { username: req.user.username },
          Object.assign(req.body, { avatarUrl: result.url })
        )
          .then(() => res.redirect("/profile"))
          .catch(err => res.status(400).send(err.toString()));
      })
      .end(fileBuffer);
  } else {
    User.findOneAndUpdate(
      { username: req.user.username },
      Object.assign(req.body)
    )
      .then(() => res.redirect("/profile"))
      .catch(err => res.status(400).send(err.toString()));
  }
});

module.exports = router;
