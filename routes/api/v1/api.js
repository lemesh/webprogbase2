const express = require("express");
const passport = require("passport");
const router = express.Router();
const bcrypt = require("bcryptjs");

const User = require("../../../models/User");
const Post = require("../../../models/Post");
const Comment = require("../../../models/Comment");

router.get("/", (req, res) => {
  res.json({});
});

router.get(
  "/me",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    res.json(req.user);
  }
);

// -----------------------------------------------------------------------------
// Users -----------------------------------------------------------------------
// -----------------------------------------------------------------------------

router.get(
  "/users",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    const page = req.query.page || 1;
    const par = {
      page,
      limit: 10
    };
    User.paginate({}, par)
      .then(users => res.status(200).json(users.docs))
      .catch(err => res.status(400).json(err));
  }
);

router.get(
  "/users/:username",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    User.findOne({ username: req.params.username })
      .then(user => {
        if (!user) {
          return Promise.reject(
            res.status(404).json({ error: { username: "No such username" } })
          );
        }
        return res.status(200).json(user);
      })
      .catch(err => err);
  }
);

router.post(
  "/users/add",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    const {
      username,
      password,
      fullname,
      email,
      confirm,
      role,
      registeredAt,
      avatarUrl,
      isDisabled,
      posts
    } = req.body;
    const error = {};
    if (!email) {
      error.email = "Email is required";
    }
    if (registeredAt) {
      error.registeredAt = "You cannot create this field";
    }
    if (avatarUrl) {
      error.avatarUrl = "You cannot create this field";
    }
    if (isDisabled) {
      error.isDisabled = "You cannot create this field";
    }
    if (posts) {
      error.posts = "You cannot create this field";
    }
    if (!fullname) {
      error.fullname = "Fullname is required";
    }
    if (!password) {
      error.password = "Password is required";
    }
    if (!confirm) {
      error.confirm = "Confirm is required";
    }
    if (!username) {
      error.username = "Username is required";
    }
    if (Object.keys(error).length !== 0) return res.status(400).json(error);
    User.findOne({ email })
      .then(user => {
        if (user) {
          error.email = "Email already exist";
        }
        if (Object.keys(error).length !== 0) {
          return Promise.reject(error);
        }
        return User.findOne({ username });
      })
      .then(user => {
        if (role && (+role !== 0 && +role !== 1)) {
          error.role = "No such role";
        }
        if (password && confirm && password !== confirm) {
          error.password = "Passwords don't match";
        }
        if (user) {
          error.username = "Username already exist";
        }
        if (Object.keys(error).length !== 0) return Promise.reject(error);
        return bcrypt.genSalt(10);
      })
      .then(salt => {
        return bcrypt.hash(password, salt);
      })
      .then(hash => {
        return User.create({ username, password: hash, fullname, email });
      })
      .then(user => res.status(201).json({ user, msg: "Success" }))
      .catch(err => {
        return res.status(400).json(err);
      });
  }
);

router.put(
  "/users/:user/update",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    const error = {};
    const {
      username,
      email,
      password,
      registeredAt,
      avatarUrl,
      isDisabled,
      posts
    } = req.body;
    if (registeredAt) {
      error.registeredAt = "You cannot update this field";
    }
    if (avatarUrl) {
      error.avatarUrl = "You cannot update this field";
    }
    if (isDisabled) {
      error.isDisabled = "You cannot update this field";
    }
    if (posts) {
      error.posts = "You cannot update this field";
    }
    if (Object.keys(error).length !== 0) return res.status(400).json(error);
    User.findOne({ username: req.params.user })
      .then(user => {
        if (!user) {
          error.username = "No such user";
        }
        if (Object.keys(error).length !== 0)
          return Promise.reject(res.status(404).json(error));
        return User.findOne({ username });
      })
      .then(user => {
        if (user) {
          error.username = "Username already exist";
        }
        if (Object.keys(error).length !== 0)
          return Promise.reject(res.status(400).json(error));
        return User.findOne({ email });
      })
      .then(user => {
        if (user) {
          error.email = "Email already exist";
        }
        if (Object.keys(error).length !== 0)
          return Promise.reject(res.status(400).json(error));
        const promises = [];
        if (req.body.password) {
          promises.push(bcrypt.genSalt(10));
        }
        return Promise.all(promises);
      })
      .then(([salt]) => {
        const promises = [];
        if (salt) {
          promises.push(bcrypt.hash(password, salt));
        }
        return Promise.all(promises);
      })
      .then(([hash]) => {
        if (hash) {
          req.body.password = hash;
        }
        return User.findOneAndUpdate({ username: req.params.user }, req.body, {
          new: true,
          runValidators: true
        });
      })
      .then(user => res.status(201).json({ user, msg: "Success" }))
      .catch(err => err);
  }
);

router.delete(
  "/users/:username/remove",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    User.findOneAndDelete({ username: req.params.username })
      .then(user => {
        if (!user) {
          return Promise.reject(
            res.status(404).json({ error: { username: "No such username" } })
          );
        }
        return res.status(201).json({ user, msg: "Success" });
      })
      .catch(err => err);
  }
);

// -----------------------------------------------------------------------------
// Posts -----------------------------------------------------------------------
// -----------------------------------------------------------------------------

router.get(
  "/posts",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    const page = req.query.page || 1;
    const obj = {};
    if (req.query.search) {
      obj.title = new RegExp(req.query.search, "gi");
    }
    const par = {
      page,
      limit: 10
    };
    Post.paginate(obj, par)
      .then(posts => res.json(posts))
      .catch(err => res.status(400).json(err));
  }
);

router.get(
  "/posts/:id",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    Post.findById(req.params.id)
      .then(post => {
        if (!post) {
          return Promise.reject(
            res.status(404).json({ error: { id: "No such id" } })
          );
        }
        return res.json(post);
      })
      .catch(err => err);
  }
);

router.post(
  "/posts/add",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    if (!req.body.author) {
      req.body.author = req.user._id;
    }
    const {
      content,
      title,
      author,
      likes,
      commentsCount,
      shares,
      postDate,
      photo,
      comments
    } = req.body;
    const error = {};
    if (!content) {
      error.content = "Content is required";
    }
    if (!title) {
      error.title = "Title is required";
    }
    if (likes) {
      error.likes = "You cannot add this field";
    }
    if (commentsCount) {
      error.commentsCount = "You cannot add this field";
    }
    if (shares) {
      error.shares = "You cannot add this field";
    }
    if (postDate) {
      error.postDate = "You cannot add this field";
    }
    if (photo) {
      error.photo = "You cannot add this field";
    }
    if (comments) {
      error.comments = "You cannot add this field";
    }
    if (Object.keys(error).length !== 0) return res.status(400).json(error);
    Post.create(req.body)
      .then(post => res.status(201).json({ post, msg: "Success" }))
      .catch(err => res.status(400).json(err));
  }
);

router.put(
  "/posts/:id/update",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    const {
      author,
      likes,
      commentsCount,
      shares,
      postDate,
      photo,
      comments
    } = req.body;
    const error = {};
    if (likes) {
      error.likes = "You cannot update this field";
    }
    if (commentsCount) {
      error.commentsCount = "You cannot update this field";
    }
    if (shares) {
      error.shares = "You cannot update this field";
    }
    if (postDate) {
      error.postDate = "You cannot update this field";
    }
    if (photo) {
      error.photo = "You cannot update this field";
    }
    if (comments) {
      error.comments = "You cannot update this field";
    }
    if (author) {
      error.author = "You cannot update this field";
    }
    if (Object.keys(error).length !== 0) return res.status(400).json(error);
    Post.findOneAndUpdate({ _id: req.params.id }, req.body, {
      new: true,
      runValidators: true
    })
      .then(post => {
        if (!post) {
          return Promise.reject(
            res.status(404).json({ error: { id: "No such id" } })
          );
        }
        return res.status(201).json({ post, msg: "Success" });
      })
      .catch(err => err);
  }
);

router.delete(
  "/posts/:id/remove",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    Post.findByIdAndDelete(req.params.id)
      .then(post => {
        if (!post) {
          return Promise.reject(
            res.status(404).json({ error: { id: "No such id" } })
          );
        }
        const promises = [];
        promises.push(res.status(201).json({ post, msg: "Success" }));
        post.comments.map(x => promises.push(Comment.findByIdAndRemove(x)));
        return Promise.all(promises);
      })
      .then(([post, comments]) => post)
      .catch(err => err);
  }
);

// -----------------------------------------------------------------------------
// Comments --------------------------------------------------------------------
// -----------------------------------------------------------------------------

router.get(
  "/comments",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    const page = req.query.page || 1;
    const obj = {};
    if (req.query.search) {
      obj.content = new RegExp(req.query.search, "gi");
    }
    const par = {
      page,
      limit: 10
    };
    Comment.paginate(obj, par)
      .then(comments => res.status(200).json(comments))
      .catch(err => res.status(400).json({ err }));
  }
);

router.get(
  "/comments/:id",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    Comment.findById(req.params.id)
      .then(comment => {
        if (!comment) {
          return Promise.reject(
            res.status(404).json({ error: { id: "No such id" } })
          );
        }
        return res.status(200).json(comment);
      })
      .catch(err => err);
  }
);

router.post(
  "/comments/add",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    if (!req.body.author) {
      req.body.author = req.user._id;
    }
    const { post, author, content, likes, commentDate, replies } = req.body;
    const error = {};
    if (!post) {
      error.post = "Post is required";
    }
    if (!content) {
      error.content = "Content is required";
    }
    if (likes) {
      error.likes = "You cannot add this field";
    }
    if (commentDate) {
      error.commentDate = "You cannot add this field";
    }
    if (replies) {
      error.replies = "You cannot add this field";
    }
    if (Object.keys(error).length !== 0) return res.status(400).json(error);
    Comment.create(req.body)
      .then(comment => {
        return Promise.all([comment, Post.findById(post)]);
      })
      .then(([comment, post]) => {
        if (post) {
          post.comments.push(comment._id);
          return Promise.all([comment, Post.findByIdAndUpdate(post._id, post)]);
        } else
          return Promise.reject(
            res.status(404).json({ error: { post: "No such post" } })
          );
      })
      .then(([comment, post]) => {
        return res.status(201).json({ comment, msg: "Success" });
      })
      .catch(err => res.status(400).json({ err }));
  }
);

router.put(
  "/comments/:id/update",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    const { post, author, content, likes, commentDate, replies } = req.body;
    const error = {};
    if (post) {
      error.post = "You cannot update this field";
    }
    if (author) {
      error.author = "You cannot update this field";
    }
    if (likes) {
      error.likes = "You cannot update this field";
    }
    if (commentDate) {
      error.commentDate = "You cannot update this field";
    }
    if (replies) {
      error.replies = "You cannot update this field";
    }
    if (Object.keys(error).length !== 0) return res.status(400).json(error);
    Comment.findOneAndUpdate({ _id: req.params.id }, req.body, {
      new: true,
      runValidators: true
    })
      .then(comment => {
        if (!comment) {
          return Promise.reject(
            res.status(404).json({ error: { id: "No such id" } })
          );
        }
        return res.status(201).json({ comment, msg: "Success" });
      })
      .catch(err => err);
  }
);

router.delete(
  "/comments/:id/remove",
  passport.authenticate("basic", { session: false }),
  (req, res) => {
    Comment.findByIdAndDelete(req.params.id)
      .then(comment => {
        if (!comment) {
          return Promise.reject(
            res.status(404).json({ error: { id: "No such id" } })
          );
        }
        return res.status(201).json({ comment, msg: "Success" });
      })
      .catch(err => res.status(400).json({ err }));
  }
);

module.exports = router;
