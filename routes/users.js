const express = require("express");
const router = express.Router();

const User = require("../models/User");
require("../models/Post");

function checkAdmin(req, res, next) {
  if (!req.user) return res.redirect("/auth/login");
  else if (req.user.role !== 1) res.sendStatus(403);
  else next();
}

router.get("/", checkAdmin, (req, res) => {
  User.find()
    .then(users => {
      res.render("users", { user: req.user, users, title: "Users" });
    })
    .catch(err => res.status(400).send(err.toString()));
});

router.get("/:username", checkAdmin, (req, res) => {
  User.findOne({ username: req.params.username })
    .populate({
      path: "posts",
      populate: {
        path: "comments",
        populate: { path: "author" }
      }
    })
    .exec()
    .then(user => {
      if (user) {
        const data = { userdata: user, user: req.user, title: "User" };
        if (req.user) data.title = req.user.fullname;
        res.render("user", data);
      } else {
        res.status(404).render("404", { user: req.user });
      }
    })
    .catch();
});

router.get("/:username/makeadmin", (req, res) => {
  User.findOneAndUpdate({ username: req.params.username }, { role: 1 })
    .then(() => res.redirect("/users"))
    .catch(err => res.status(400).send(err.toString()));
});

router.get("/:username/makeuser", (req, res) => {
  if (
    req.params.username !== "lemesh" &&
    req.params.username !== req.user.username
  )
    User.findOneAndUpdate({ username: req.params.username }, { role: 0 })
      .then(() => res.redirect("/"))
      .catch(err => res.status(400).send(err.toString()));
  else {
    res.redirect("/users");
  }
});

module.exports = router;
