const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const passport = require("passport");

const User = require("../models/User");

router.get("/register", (req, res) => {
  res.render("register", {
    title: "Register new user",
    error: req.query.error
  });
});

router.post("/register", (req, res) => {
  const { email, fullname, username, password, confirm } = req.body;
  User.findOne({ email })
    .then(user => {
      if (user) {
        res.redirect("/auth/register?error=Email+already+exist");
      } else {
        return User.findOne({ username }).then(user => {
          if (user) {
            res.redirect("/auth/register?error=Username+already+exist");
          } else {
            return bcrypt.genSalt(10).then(salt => {
              return bcrypt.hash(password, salt).then(hash => {
                return User.create({
                  email,
                  fullname,
                  username,
                  password: hash
                }).then(() => res.redirect("/auth/login"));
              });
            });
          }
        });
      }
    })
    .catch(err => res.status(400).send(err.toString()));
});

router.get("/login", (req, res) => {
  res.render("login", { title: "Login" });
});

router.post(
  "/login",
  passport.authenticate("local", {
    failureRedirect: "/auth/login"
  }),
  (req, res) => {
    res.redirect("/profile");
  }
);

router.post("/logout", (req, res) => {
  req.logout();
  res.redirect("/");
});

module.exports = router;
