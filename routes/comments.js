const express = require("express");
const router = express.Router();

function checkAuth(req, res, next) {
  if (!req.user) return res.redirect("/auth/login");
  next();
}

const Comment = require("../models/Comment");
const Post = require("../models/Post");

const pageInfo = {
  page: 1,
  search: ""
};

router.get("/", checkAuth, (req, res) => {
  res.render("comments", {
    title: "Comments",
    user: req.user,
    commentsActive: "active"
  });
});

router.get("/new", checkAuth, (req, res) => {
  Post.find()
    .then(posts => {
      res.render("newComment", {
        title: "New comment",
        user: req.user,
        posts
      });
    })
    .catch(err => res.status(400).send(err.toString()));
});

router.post("/new", checkAuth, (req, res) => {
  Comment.create(Object.assign(req.body, { author: req.user._id }))
    .then(comment => {
      return Post.findById(comment.post).then(post => {
        post.comments.push(comment._id);
        return Post.findByIdAndUpdate(comment.post, post).then(() =>
          res.redirect(`/posts/${comment.post}`)
        );
      });
    })
    .catch(err => res.status(400).send(err.toString()));
});

router.post("/add", checkAuth, (req, res) => {
  Comment.create(Object.assign(req.body, { author: req.user._id }))
    .then(comment => {
      return Post.findById(comment.post).then(post => {
        post.comments.push(comment._id);
        return Post.findByIdAndUpdate(comment.post, post)
          .populate("author")
          .then(post => res.redirect(`/users/${post.author.username}`));
      });
    })
    .catch(err => res.status(400).send(err.toString()));
});

router.post("/delete", checkAuth, (req, res) => {
  Comment.findByIdAndRemove(req.body.comment)
    .then(() => res.redirect("/comments"))
    .catch(err => res.status(400).send(err.toString()));
});

module.exports = router;
