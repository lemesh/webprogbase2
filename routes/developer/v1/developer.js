const express = require("express");
const router = express.Router();
const fs = require("fs-extra");
const path = require("path");

const components = [
  {
    name: "Users",
    routes: [
      {
        get: 1,
        format: "GET",
        path: "/users",
        desc: "Get all users",
        responses: [
          {
            status: "200",
            desc: "Successful operation, return object with users",
            code: `[
    {
        "role": 1,
        "avatarUrl": "https://res.cloudinary.com/drv2azi1q/image/upload/v1542146238/avatar.png",
        "isDisabled": false,
        "posts": [],
        "_id": "5bebf6ce05c1ac07d98cbec1",
        "email": "lemesh2424@gmail.com",
        "fullname": "Олег Лемешенко",
        "username": "lemesh",
        "password": "$2a$10$NHB45SHQVsLFZrk6sTmIbOEdK6b4C/xRzVcKwHlK0FvEFpqOcrO7W",
        "registeredAt": "2018-11-14T10:19:58.197Z",
        "__v": 0
    },
    ...
]`
          }
        ]
      },
      {
        get: 1,
        format: "GET",
        path: "/users/:username",
        desc: "Get user by username",
        parameters: [
          {
            required: true,
            name: "username",
            desc: "Username of user you want to get"
          }
        ],
        responses: [
          {
            status: "200",
            desc: "Successful operation, return object with user",
            code: `[
    {
        "role": 1,
        "avatarUrl": "https://res.cloudinary.com/drv2azi1q/image/upload/v1542146238/avatar.png",
        "isDisabled": false,
        "posts": [],
        "_id": "5bebf6ce05c1ac07d98cbec1",
        "email": "lemesh2424@gmail.com",
        "fullname": "Олег Лемешенко",
        "username": "lemesh",
        "password": "$2a$10$NHB45SHQVsLFZrk6sTmIbOEdK6b4C/xRzVcKwHlK0FvEFpqOcrO7W",
        "registeredAt": "2018-11-14T10:19:58.197Z",
        "__v": 0
    }
]`
          },
          {
            status: "404",
            desc: "User not found",
            code: `{
    "error": {
        "username": "No such username"
    }
}`
          }
        ]
      },
      {
        post: 1,
        format: "POST",
        path: "/users/add",
        desc: "Add new user",
        parameters: [
          {
            required: true,
            name: "email",
            desc: "Email of new user - unique"
          },
          {
            required: true,
            name: "fullname",
            desc: "Fullname of new user"
          },
          {
            required: true,
            name: "password",
            desc: "Password of new user - will be hashed"
          },
          {
            required: true,
            name: "confirm",
            desc: "Confirmation of password - need be the same with password"
          },
          {
            required: true,
            name: "username",
            desc: "Username of new user - unique"
          },
          {
            required: false,
            name: "role",
            desc: "Role of new user",
            values: "0 - user, 1 - admin"
          }
        ],
        responses: [
          {
            status: "201",
            desc: "Successful operation, return added user",
            code: `{
    "user": {
        "role": 0,
        "avatarUrl": "https://res.cloudinary.com/drv2azi1q/image/upload/v1542146238/avatar.png",
        "isDisabled": false,
        "posts": [],
        "_id": "5bee3747313f567b4464717d",
        "username": "mypaxa",
        "password": "$2a$10$GziGItsprnbbxEylkLWGYurgo6gbGqlc.8sXiCvplUUphwsz4Pbji",
        "fullname": "Artem Murashko",
        "email": "mypaxa@gmail.com",
        "registeredAt": "2018-11-16T03:19:35.568Z",
        "__v": 0
    },
    "msg": "Success"
}`
          },
          {
            status: "400",
            desc: "Bad request, return object with errors",
            code: `{
    "email": "Email is required",
    "fullname": "Fullname is required",
    "password": "Password is required",
    "confirm": "Confirm is required",
    "username": "Username is required"
}`
          }
        ]
      },
      {
        put: 1,
        format: "PUT",
        path: "/users/:user/update",
        desc: "Update user by username",
        parameters: [
          {
            required: true,
            name: "user",
            desc: "Username of user you want to update"
          },
          {
            required: false,
            name: "username",
            desc: "Username you want to update"
          },
          {
            required: false,
            name: "email",
            desc: "Email you want to update"
          },
          {
            required: false,
            name: "password",
            desc: "Password you want to update"
          }
        ],
        responses: [
          {
            status: "201",
            desc: "Successful operation, return updated user",
            code: `{
    "user": {
        "role": 1,
        "avatarUrl": "https://res.cloudinary.com/drv2azi1q/image/upload/v1542146238/avatar.png",
        "isDisabled": false,
        "posts": [],
        "_id": "5bee3747313f567b4464717d",
        "username": "mypaxa",
        "password": "$2a$10$GziGItsprnbbxEylkLWGYurgo6gbGqlc.8sXiCvplUUphwsz4Pbji",
        "fullname": "Artem Murashko",
        "email": "mypaxa@gmail.com",
        "registeredAt": "2018-11-16T03:19:35.568Z",
        "__v": 0
    },
    "msg": "Success"
}`
          },
          {
            status: "400",
            desc: "Bad request, return object with errors",
            code: `{
    "username": "Username already exist"
}`
          },
          {
            status: "404",
            desc: "User not found",
            code: `{
    "username": "No such user"
}`
          }
        ]
      },
      {
        delete: 1,
        format: "DELETE",
        path: "/users/:username/remove",
        desc: "Remove user by username",
        parameters: [
          {
            required: true,
            name: "username",
            desc: "Username of user you want to delete"
          }
        ],
        responses: [
          {
            status: "200",
            desc: "Successful operation, return removed user",
            code: `{
    "user": {
        "role": 1,
        "avatarUrl": "https://res.cloudinary.com/drv2azi1q/image/upload/v1542146238/avatar.png",
        "isDisabled": false,
        "posts": [],
        "_id": "5bee3747313f567b4464717d",
        "username": "mypaxa",
        "password": "$2a$10$GziGItsprnbbxEylkLWGYurgo6gbGqlc.8sXiCvplUUphwsz4Pbji",
        "fullname": "Artem Murashko",
        "email": "mypaxa@gmail.com",
        "registeredAt": "2018-11-16T03:19:35.568Z",
        "__v": 0
    },
    "msg": "Success"
}`
          },
          {
            status: "404",
            desc: "User not found",
            code: `{
    "error": {
        "username": "No such username"
    }
}`
          }
        ]
      }
    ]
  },
  {
    name: "Posts",
    routes: [
      {
        get: 1,
        format: "GET",
        path: "/posts",
        desc: "Get all posts",
        parameters: [
          {
            required: false,
            name: "page",
            desc: "Page of post's list (default: 1)"
          }
        ],
        responses: [
          {
            status: "200",
            desc: "Successful operation, return object with posts",
            code: `[
    {
        "likes": 0,
        "commentsCount": 0,
        "shares": 0,
        "comments": [],
        "_id": "5bec8783096899377a5c808f",
        "photos": [],
        "title": "Post #100 new",
        "content": "Hello",
        "author": "5bebf6ce05c1ac07d98cbec1",
        "postDate": "2018-11-14T20:37:23.523Z",
        "__v": 0,
        "photo": "http://res.cloudinary.com/drv2azi1q/raw/upload/v1542235161/rwjr0vpqmmyh3i3kihza"
    },
    ...
]`
          }
        ]
      },
      {
        get: 1,
        format: "GET",
        path: "/posts/:id",
        desc: "Get post by id",
        parameters: [
          {
            required: true,
            name: "id",
            desc: "Id of post you want to get"
          }
        ],
        responses: [
          {
            status: "200",
            desc: "Successful operation",
            code: `{
    "likes": 0,
    "commentsCount": 0,
    "shares": 0,
    "comments": [
        "5beca3d0f2c7dc5210647c0d",
        "5beca4e18585db536d516ebe",
        "5beca65d89ec0f5581d7bf71",
        "5beca6816f2dca55ead99abe",
        "5beca6ab818f6f5656783c9b",
        "5beca6d5818f6f5656783c9d"
    ],
    "_id": "5bec8783096899377a5c808f",
    "photos": [],
    "title": "Post #100 new",
    "content": "Hello",
    "author": "5bebf6ce05c1ac07d98cbec1",
    "postDate": "2018-11-14T20:37:23.523Z",
    "__v": 0,
    "photo": "http://res.cloudinary.com/drv2azi1q/raw/upload/v1542235161/rwjr0vpqmmyh3i3kihza"
}`
          },
          {
            status: "404",
            desc: "Post not found",
            code: `{
    "error": {
        "id": "No such id"
    }
}`
          }
        ]
      },
      {
        post: 1,
        format: "POST",
        path: "/posts/add",
        desc: "Add new post",
        parameters: [
          {
            required: true,
            name: "content",
            desc: "Content of new post"
          },
          {
            required: true,
            name: "title",
            desc: "Title of new post"
          },
          {
            required: false,
            name: "author",
            desc: "Author of the post (default: Authorized user id)"
          }
        ],
        responses: [
          {
            status: "201",
            desc: "Successful operation, return added post",
            code: `{
    "post": {
        "likes": 0,
        "commentsCount": 0,
        "shares": 0,
        "comments": [],
        "_id": "5bee429d43065d12db91c074",
        "content": "New post, very funny",
        "title": "Hahahah",
        "author": "5bebf6ce05c1ac07d98cbec1",
        "postDate": "2018-11-16T04:07:57.330Z",
        "__v": 0
    },
    "msg": "Success"
}`
          },
          {
            status: "400",
            desc: "Bad request, return object with errors",
            code: `{
    "content": "Content is required",
    "title": "Title is required"
}`
          }
        ]
      },
      {
        put: 1,
        format: "PUT",
        path: "/posts/:id/update",
        desc: "Update post by id",
        parameters: [
          {
            required: true,
            name: "id",
            desc: "Id of post you want to update"
          },
          {
            required: false,
            name: "content",
            desc: "Content you want to update"
          },
          {
            required: false,
            name: "title",
            desc: "Title you want to update"
          }
        ],
        responses: [
          {
            status: "201",
            desc: "Successful operation, return updated post",
            code: `{
    "post": {
        "likes": 0,
        "commentsCount": 0,
        "shares": 0,
        "comments": [
            "5beca3d0f2c7dc5210647c0d",
            "5beca4e18585db536d516ebe",
            "5beca65d89ec0f5581d7bf71",
            "5beca6816f2dca55ead99abe",
            "5beca6ab818f6f5656783c9b",
            "5beca6d5818f6f5656783c9d"
        ],
        "_id": "5bec8783096899377a5c808f",
        "photos": [],
        "title": "Post #100 new",
        "content": "New post, very funny, too",
        "author": "5bebf6ce05c1ac07d98cbec1",
        "postDate": "2018-11-14T20:37:23.523Z",
        "__v": 0,
        "photo": "http://res.cloudinary.com/drv2azi1q/raw/upload/v1542235161/rwjr0vpqmmyh3i3kihza"
    },
    "msg": "Success"
}`
          },
          {
            status: "404",
            desc: "Post not found",
            code: `{
    "id": "No such id"
}`
          }
        ]
      },
      {
        delete: 1,
        format: "DELETE",
        path: "/posts/:id/remove",
        desc: "Remove post by id",
        parameters: [
          {
            required: true,
            name: "id",
            desc: "Id of post you want to delete"
          }
        ],
        responses: [
          {
            status: "201",
            desc: "Successful operation, return removed post",
            code: `{
    "post": {
        "likes": 0,
        "commentsCount": 0,
        "shares": 0,
        "comments": [
            "5beca3d0f2c7dc5210647c0d",
            "5beca4e18585db536d516ebe",
            "5beca65d89ec0f5581d7bf71",
            "5beca6816f2dca55ead99abe",
            "5beca6ab818f6f5656783c9b",
            "5beca6d5818f6f5656783c9d"
        ],
        "_id": "5bec8783096899377a5c808f",
        "photos": [],
        "title": "Post #100 new",
        "content": "New post, very funny, too",
        "author": "5bebf6ce05c1ac07d98cbec1",
        "postDate": "2018-11-14T20:37:23.523Z",
        "__v": 0,
        "photo": "http://res.cloudinary.com/drv2azi1q/raw/upload/v1542235161/rwjr0vpqmmyh3i3kihza"
    },
    "msg": "Success"
}`
          },
          {
            status: "404",
            desc: "Post not found",
            code: `{
    "error": {
        "id": "No such id"
    }
}`
          }
        ]
      }
    ]
  },
  {
    name: "Comments",
    routes: [
      {
        get: 1,
        format: "GET",
        path: "/comments",
        desc: "Get all comments",
        parameters: [
          {
            required: false,
            name: "page",
            desc: "Page of comment's list (default: 1)"
          }
        ],
        responses: [
          {
            status: "200",
            desc: "Successful operation, return object with comments",
            code: `[
    {
        "likes": 0,
        "replies": [],
        "_id": "5beca3d0f2c7dc5210647c0d",
        "post": "5bec8783096899377a5c808f",
        "content": "dsadsad",
        "author": "5bebf6ce05c1ac07d98cbec1",
        "commentDate": "2018-11-14T22:38:08.140Z",
        "__v": 0
    },
    ...
]`
          }
        ]
      },
      {
        get: 1,
        format: "GET",
        path: "/comments/:id",
        desc: "Get comment by id",
        parameters: [
          {
            required: true,
            name: "id",
            desc: "Id of comment you want to get"
          }
        ],
        responses: [
          {
            status: "200",
            desc: "Successful operation",
            code: `{
    "likes": 0,
    "replies": [],
    "_id": "5beca3d0f2c7dc5210647c0d",
    "post": "5bec8783096899377a5c808f",
    "content": "dsadsad",
    "author": "5bebf6ce05c1ac07d98cbec1",
    "commentDate": "2018-11-14T22:38:08.140Z",
    "__v": 0
}`
          },
          {
            status: "404",
            desc: "Comment not found",
            code: `{
    "error": {
        "id": "No such id"
    }
}`
          }
        ]
      },
      {
        post: 1,
        format: "POST",
        path: "/comments/add",
        desc: "Add new comment",
        parameters: [
          {
            required: true,
            name: "post",
            desc: "Id of post commented in"
          },
          {
            required: true,
            name: "content",
            desc: "Content of new comment"
          },
          {
            required: false,
            name: "author",
            desc: "Author of the comment (default: Authorized user id)"
          }
        ],
        responses: [
          {
            status: "201",
            desc: "Successful operation, return added comment",
            code: `{
    "comment": {
        "likes": 0,
        "replies": [],
        "_id": "5bee46374b2bc41b204a4831",
        "content": "Nice comment",
        "post": "5bec8783096899377a5c808f",
        "author": "5bebf6ce05c1ac07d98cbec1",
        "commentDate": "2018-11-16T04:23:19.380Z",
        "__v": 0
    },
    "msg": "Success"
}`
          },
          {
            status: "400",
            desc: "Bad request, return object with errors",
            code: `{
    "post": "Post is required",
    "content": "Content is required"
}`
          },
          {
            status: "404",
            desc: "Comment not found",
            code: `{
    "error": {
        "post": "No such post"
    }
}`
          }
        ]
      },
      {
        put: 1,
        format: "PUT",
        path: "/comments/:id/update",
        desc: "Update comment by id",
        parameters: [
          {
            required: true,
            name: "id",
            desc: "Id of comment you want to update"
          },
          {
            required: false,
            name: "content",
            desc: "Content you want to update"
          }
        ],
        responses: [
          {
            status: "201",
            desc: "Successful operation, return updated comment",
            code: `{
    "comment": {
        "likes": 0,
        "replies": [],
        "_id": "5bee46374b2bc41b204a4831",
        "content": "Nice comment, very",
        "post": "5bec8783096899377a5c808f",
        "author": "5bebf6ce05c1ac07d98cbec1",
        "commentDate": "2018-11-16T04:23:19.380Z",
        "__v": 0
    },
    "msg": "Success"
}`
          },
          {
            status: "404",
            desc: "Comment not found",
            code: `{
    "id": "No such id"
}`
          }
        ]
      },
      {
        delete: 1,
        format: "DELETE",
        path: "/comments/:id/remove",
        desc: "Remove comment by id",
        parameters: [
          {
            required: true,
            name: "id",
            desc: "Id of comment you want to delete"
          }
        ],
        responses: [
          {
            status: "201",
            desc: "Successful operation, return removed comment",
            code: `{
    "comment": {
        "likes": 0,
        "replies": [],
        "_id": "5bee46374b2bc41b204a4831",
        "content": "Nice comment, very",
        "post": "5bec8783096899377a5c808f",
        "author": "5bebf6ce05c1ac07d98cbec1",
        "commentDate": "2018-11-16T04:23:19.380Z",
        "__v": 0
    },
    "msg": "Success"
}`
          },
          {
            status: "404",
            desc: "Comment not found",
            code: `{
    "error": {
        "id": "No such id"
    }
}`
          }
        ]
      }
    ]
  }
];

router.get("/", (req, res) => {
  res.render("developer", {
    title: "Documentation API",
    components,
    user: req.user
  });
});

module.exports = router;
