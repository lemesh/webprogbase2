const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const { Schema } = mongoose;

const PostSchema = new Schema({
  content: { type: String, required: true },
  title: { type: String, required: true },
  author: { type: Schema.Types.ObjectId, ref: "User", required: true },
  likes: { type: Number, min: 0, default: 0 },
  commentsCount: { type: Number, min: 0, default: 0 },
  shares: { type: Number, min: 0, default: 0 },
  postDate: { type: Date, default: Date.now },
  photo: { type: String },
  comments: [{ type: Schema.Types.ObjectId, ref: "Comment" }]
});

PostSchema.plugin(mongoosePaginate);
const Post = mongoose.model("Post", PostSchema);
module.exports = Post;
