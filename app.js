const express = require("express");
const bodyParser = require("body-parser");
const busboyBodyParser = require("busboy-body-parser");
const mustache = require("mustache-express");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const cookieParser = require("cookie-parser");
const session = require("express-session");
const mongoose = require("mongoose");
const path = require("path");
const BasicStrategy = require("passport-http").BasicStrategy;
const bcrypt = require("bcryptjs");

const app = express();

const authRouter = require("./routes/auth");
const usersRouter = require("./routes/users");
const postsRouter = require("./routes/posts");
const commentsRouter = require("./routes/comments");
const apiRouter = require("./routes/api/v1/api");
const developerRouter = require("./routes/developer/v1/developer");
const profileRouter = require("./routes/profile");

const User = require("./models/User");

const config = require("./config/config");

app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(busboyBodyParser({ limit: "5mb" }));
app.use(cookieParser());
app.use(
  session({
    secret: config.passport.secret,
    resave: false,
    saveUninitialized: true
  })
);

app.use(passport.initialize());
app.use(passport.session());
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "mst");
app.engine("mst", mustache(path.join(__dirname, "views/partials")));

passport.serializeUser((user, done) => {
  done(null, user._id);
});

passport.deserializeUser((id, done) => {
  User.findById(id)
    .then(user => done(null, user))
    .catch(err => done(err));
});

passport.use(
  new BasicStrategy(function(userid, password, done) {
    User.findOne({ username: userid }, function(err, user) {
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(null, false);
      }
      bcrypt
        .compare(password, user.password)
        .then(res => {
          if (res) {
            return done(null, user);
          }
          return done(null, false);
        })
        .catch(err => done(err));
    });
  })
);

passport.use(
  new LocalStrategy((username, password, done) => {
    User.findOne({ username })
      .then(user => {
        if (user) {
          return bcrypt.compare(password, user.password).then(res => {
            if (res) return done(null, user);
            else return done(null, false, { err: "Password doesn't correct" });
          });
        } else {
          return done(null, false, { err: "Email doesn't exist" });
        }
      })
      .catch(err => done(err));
  })
);

app.get("/", (req, res) => {
  res.render("index", {
    title: "Look At!",
    user: req.user,
    homeActive: "active"
  });
});

app.use("/auth", authRouter);
app.use("/users", usersRouter);
app.use("/posts", postsRouter);
app.use("/comments", commentsRouter);
app.use("/api/v1", apiRouter);
app.use("/developer/v1", developerRouter);
app.use("/profile", profileRouter);

app.get("/about", (req, res) => {
  res.render("about", {
    title: "About Look At!",
    user: req.user,
    aboutActive: "active"
  });
});

app.get("/*", (req, res) => {
  res.render("404");
});

const port = process.env.PORT || 3000;

mongoose
  .connect(
    config.databaseURL,
    { useNewUrlParser: true }
  )
  .then(() => console.log(`Database connected: ${config.databaseURL}`))
  .then(() =>
    app.listen(port, () => console.log(`Server started on port ${port}`))
  )
  .catch(err => console.error(`Start error: ${err}`));
